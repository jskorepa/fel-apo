/*
 * Copyright <2017> <Jakub Skořepa, Klára Jakešová>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "draw.h"

#ifdef DESKTOP
SDL_Surface * surface;
SDL_Texture * texture;
SDL_Renderer * renderer;
#endif

void Framebuffer::draw_rectangle(int x1, int y1, int x2, int y2, uint16_t color)
{
  for(int i = x1; i < x2; i++) {
    for(int j = y1; j < y2; j++) {
      fb[i][j] = color;
    }
  }
}

namespace {
    int square(int a)
    {
      return a*a;
    }
}

void Framebuffer::draw_circle(int centerX, int centerY, int radius, uint16_t color)
{
  for (int y = 0; y < HEIGHT; y++) {
    for (int x = 0; x < WIDTH; x++) {
      if(square(y-centerY) + square(x - centerX) <= square(radius)) {
        fb[x][y] = color;
      }
    }
  }
}

namespace {
    bool character_get_at(font_descriptor_t* font, int x, int y, char c) {
        return font->bits[font->height * c + y] & (1 << (font->maxwidth - x + 1));
    }
}

unsigned char Framebuffer::draw_character(font_descriptor_t* font, int posX, int posY, char char_id, uint16_t color)
{
  char c = char_id - font->firstchar;
  int width = font->width == 0 ? font->maxwidth : font->width[(int) c];
  for(int y = 0; y < font->height; y++) {
    for(int x = 0; x < width; x++) {
      if(character_get_at(font, x, y, c)) {
        fb[x + posX][y + posY] = color;
      } else {
        //fb[x + posX][y + posY] = 0;
      }
    }
  }
  return width;
}

unsigned char Framebuffer::draw_character_scaled(font_descriptor_t* font, int posX, int posY, char char_id, uint16_t color, uint8_t scale)
{
  char c = char_id - font->firstchar;
  int width = font->width == 0 ? font->maxwidth : font->width[(int) c];
  for(int y = 0; y < font->height*scale; y++) {
    for(int x = 0; x < width*scale; x++) {
      if(character_get_at(font, x/scale, y/scale, c)) {
        fb[x + posX][y + posY] = color;
      }
    }
  }
  return width*scale;
}

uint16_t Framebuffer::string_width(font_descriptor_t* font, const char *s, uint8_t scale)
{
  uint16_t ret = 0;
  for(; *s != 0; s++) {
    char c = *s - font->firstchar;
    ret += font->width == 0 ? font->maxwidth : font->width[(int) c];
  }
  return ret * scale;
}

void Framebuffer::draw_centered_line(font_descriptor_t* font, int posY, const char *s, uint16_t color, uint8_t scale)
{
  uint16_t x = (WIDTH - string_width(font, s, scale)) /2;
  draw_string_scaled(font, x, posY, s, color, scale);
}

int Framebuffer::draw_string(font_descriptor_t* font, int posX, int posY, const char *s, uint16_t color)
{
  for(; *s != 0; s++) {
    posX += draw_character(font, posX, posY, *s, color);
  }
  return posX;
}

int Framebuffer::draw_string_scaled(font_descriptor_t* font, int posX, int posY, const char *s, uint16_t color, uint8_t scale)
{
  for(; *s != 0; s++) {
    posX += draw_character_scaled(font, posX, posY, *s, color, scale);
  }
  return posX;
}

void Framebuffer::fill_black()
{
  for (int i = 0; i < HEIGHT ; i++) {
    for (int j = 0; j < WIDTH ; j++) {
      fb[j][i] = 0;
    }
  }
}

void Framebuffer::write_to_lcd()
{
#ifndef DESKTOP
  parlcd_write_cmd(parlcd_mem_base, 0x2c);
  for (int i = 0; i < HEIGHT; i++) {
    for (int j = 0; j < WIDTH; j++) {
      parlcd_write_data(parlcd_mem_base, fb[j][i]);
    }
  }
#else
  SDL_SetRenderTarget(renderer, texture);

  for (int i = 0; i < HEIGHT; i++) {
    for (int j = 0; j < WIDTH; j++) {
      SDL_SetRenderDrawColor(renderer,
        GET_RED(fb[j][i]) | 7, GET_GREEN(fb[j][i]) | 7, GET_BLUE(fb[j][i]) | 7,
        255);
      SDL_RenderDrawPoint(renderer, j, i);
    }
  }

  SDL_SetRenderTarget(renderer, NULL);
  SDL_RenderCopy(renderer, texture, NULL, NULL);
  SDL_RenderPresent(renderer);
#endif
}
