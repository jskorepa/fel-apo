/*
 * Copyright <2017> <Jakub Skořepa, Klára Jakešová>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <deque>
#include <vector>
#include <list>
#include <iostream>
#include "screen.h"
#include "net.h"

//////////////////////////// board
#define BOARD_NOTHING 0
#define BOARD_PLAYER_1 1
#define BOARD_PLAYER_2 2
#define BOARD_PLAYER_3 3
#define BOARD_PLAYER_4 4
#define BOARD_PLAYER_5 5
#define BOARD_PLAYER_6 6
#define BOARD_PLAYER_7 7
#define BOARD_FOOD 127

#define DIRECTION_UP 1
#define DIRECTION_RIGHT 2
#define DIRECTION_DOWN 3
#define DIRECTION_LEFT 4

/**
 * @brief Represents x,y position
 */
class GamePosition {
public:
    uint16_t x;
    uint16_t y;
};

/**
 * @brief Player of the game
 */
class GamePlayer {
public:
    uint16_t color;
    uint8_t direction;
    uint16_t length;
    bool alive;
    bool was_alive;

    // circular buffer
    std::deque<GamePosition> positions;

    GamePlayer(int i, uint16_t board_width, uint16_t board_height) {
        alive = true;
        was_alive = true;
        direction = 0;

        length = 1;

        GamePosition head;
        // assign each player unique color
        if(i == 0) { // top left
            color = RED;
            head.x = 1;
            head.y = 1;
            direction = DIRECTION_RIGHT;
        } else if(i == 1) { // bottom right
            color = GREEN;
            head.x = board_width - 2;
            head.y = board_height - 2;
            direction = DIRECTION_LEFT;
        } else if(i == 2) { // bottom left
            color = BLUE;
            head.x = 1;
            head.y = board_height - 2;
            direction = DIRECTION_UP;
        } else if(i == 3) { // top right
            color = RGB(255,255,0);
            head.x = board_width - 2;
            head.y = 1;
            direction = DIRECTION_DOWN;
        } else if(i == 4) { // top middle
            color = RGB(255,0,255);
            head.x = board_width / 2;
            head.y = 1;
            direction = DIRECTION_DOWN;
        } else if(i == 5) { // bottom middle
            color = RGB(0,255,255);
            head.x = board_width / 2 + 1;
            head.y = board_height - 2;
            direction = DIRECTION_UP;
        } else if(i == 6) { // middle
            color = RGB(255,255,255);
            head.x = board_width / 2;
            head.y = board_height / 2;
            direction = DIRECTION_LEFT;
        } else { // middle + 1
            color = RGB(100,100,255);
            head.x = board_width / 2 + 1;
            head.y = board_height / 2 + 1;
            direction = DIRECTION_RIGHT;
        }
        positions.push_front(head);
    }

    void control(knobs *knobs)
    {
        while(knobs->blue >= 4) {
            knobs->blue -= 4;
            direction++;
        }

        while(knobs->blue <= -4) {
            knobs->blue += 4;
            direction--;
        }

        while(direction < 1) {
            direction += 4;
        }

        while(direction > 4) {
            direction -= 4;
        }
    }

    void control_random()
    {
        uint8_t n = rand() % 15;
        if(n == 0) direction++;
        else if(n == 1) direction--;

        while(direction < 1) {
            direction += 4;
        }

        while(direction > 4) {
            direction -= 4;
        }
    }

    void update(uint16_t board_width, uint16_t board_height, bool grow)
    {
        if(!alive) return;
        GamePosition& oldHead = positions.at(0);
        GamePosition newHead;
        switch(direction) {
            case DIRECTION_UP:
                newHead.x = oldHead.x;
                newHead.y = oldHead.y-1;
                if(newHead.y >= board_height) newHead.y = board_height - 1;
                break;
            case DIRECTION_DOWN:
                newHead.x = oldHead.x;
                newHead.y = oldHead.y+1;
                if(newHead.y >= board_height) newHead.y = 0;
                break;
            case DIRECTION_LEFT:
                newHead.x = oldHead.x-1;
                if(newHead.x >= board_width) newHead.x = board_width - 1;
                newHead.y = oldHead.y;
                break;
            case DIRECTION_RIGHT:
                newHead.x = oldHead.x+1;
                if(newHead.x >= board_width) newHead.x = 0;
                newHead.y = oldHead.y;
                break;
        }
        positions.push_front(newHead);

        if(!grow) positions.pop_back();
    }
};

/**
 * @brief The Game
 */
class Game {
public:
    // players
    std::vector<GamePlayer> players;

    // board
    uint16_t board_width;
    uint16_t board_height;
    std::vector<uint8_t> board;

    // other
    uint16_t start_length;

    uint64_t frames;
    uint16_t food_color;
    uint16_t bg_color;
    uint8_t me_id; // id in players list

    Game(uint8_t player_count, uint16_t board_width, uint16_t board_height) {
        for(int i = 0; i < player_count; i++) {
            players.push_back(GamePlayer(i, board_width, board_height));
        }

        // other
        food_color = RGB(255,255,255);
        bg_color = RGB(63, 63, 63);
        me_id = 0;
        frames = 0;

        // initialize board
        this->board_height = board_height;
        this->board_width = board_width;

        for(int i = 0; i < board_width*board_height; i++) {
            board.push_back(BOARD_NOTHING);
        }
    }

    /**
     * Draw board to framebuffer
     * @arg fb ... framebuffer
     * @arg x, y ... where to draw
     * @arg square_size ... size of one square
     */
    void draw(Framebuffer& fb, int _x, int _y, int _square_size)
    {
        for(int y = 0; y < board_height; y++) {
            for(int x = 0; x < board_width; x++) {
                uint16_t color = bg_color;
                bool is_head = false;

                for(int i = 0; i < players.size(); i++) {
                    if(board_get(x, y) == i + 1) {
                        color = players[i].color;
                    } else if (board_get(x, y) == ((i + 1) | 0x80) ) {
                        color = players[i].color;
                        is_head = true;
                    }
                }
                if(board_get(x, y) == BOARD_FOOD) {
                    color = food_color;
                }
                fb.draw_rectangle(
                    _x + x    *_square_size,     _y + y    *_square_size,
                    _x + (x+1)*_square_size - 1, _y + (y+1)*_square_size - 1,
                    color
                );
                if(is_head) {
                    fb.draw_rectangle(
                        _x + x    *_square_size + 2,     _y + y    *_square_size + 2,
                        _x + (x+1)*_square_size - 3, _y + (y+1)*_square_size - 3,
                        0
                    );
                }
            }
        }
    }

    /**
     * Updates game by one frame
     *
     */
    void update()
    {
        frames++;
        for(int id = 0; id < players.size(); id++) {
            players[id].update(board_width, board_height, frames < start_length);
        }

        // reset board to zeroes
        for(int i = 0; i < board_width*board_height; i++) {
            board[i] = 0;
        }

        // add everything to board, except heads
        for(int id = 0; id < players.size(); id++) {
            GamePlayer &p = players.at(id);
            for(int j = 1; j < p.positions.size(); j++) {
                GamePosition &pos = p.positions.at(j);

                board_set(pos.x, pos.y, id + 1);
            }
        }

        // check heads for collision, and add them to board
        for(uint8_t id = 0; id < players.size(); id++) {
            GamePlayer& p = players.at(id);
            if(!p.alive) continue;
            GamePosition& head = p.positions.at(0);
            if(board_get(head.x, head.y) != 0) {
                p.alive = false;
            } else {
                board_set(head.x, head.y, (id + 1) | 0x80);
            }
        }
    }
private:
    uint8_t board_get(uint16_t x, uint16_t y) {
        return board[x + y*board_width];
    }

    void board_set(uint16_t x, uint16_t y, uint8_t value) {
        board[x + y*board_width] = value;
    }
};

/**
 * Start game
 * @arg fb ... framebuffer
 * @returns id of next screen
 */
int screen_singleplayer(Framebuffer& fb)
{
    Game game(8,48,32);
    game.start_length = 10;

    knobs knobs;
    knobs_init(&knobs);
    for(;;) {
        knobs_update(&knobs);
        game.players.at(0).control(&knobs);
        for(int i = 1; i < game.players.size(); i++) {
            game.players.at(i).control_random();
        }

        if(!game.players.at(0).alive) return SCREEN_SINGLE_YOU_DIED;
        bool hasAlivePlayer = false;
        for(int i = 1; i < game.players.size(); i++) {
            if(game.players.at(i).alive) hasAlivePlayer = true;
        }
        if(!hasAlivePlayer) return SCREEN_SINGLE_YOU_WON;

        fb.fill_black();
        game.update();
        game.draw(fb, 45, 30, 8);
        fb.write_to_lcd();
        sleep_ms(100);
    }
}
/**
 * Select random name from list of animals
 * @returns random name
 */
const char *random_name()
{
    const char *names[] = {
        "bittern",
        "blackbird",
        "blackfish",
        "bobolink",
        "buck",
        "buffalo",
        "bullocks",
        "cardinal",
        "cockatoo",
        "cod",
        "cur",
        "dolphin",
        "dotterel",
        "dunlin",
        "gerbil",
        "giraffe",
        "goosander",
        "hedgehog",
        "cheetah",
        "ibexe",
        "jackrabbit",
        "jellyfish",
        "lark",
        "leopard",
        "leveret",
        "llama",
        "mackerel",
        "minnow",
        "otter",
        "pelican",
        "pony",
        "raven",
        "sandpiper",
        "seahorse",
        "shads",
        "sheep",
        "shrimp",
        "snipe",
        "teal",
        "viper",
        "vole",
        "walrus",
        "waterfowl",
        "zebra"
    };
    return names[rand()%(sizeof(names) / sizeof(char*))];
}

/**
 * Show screen with posibility set name
 * @arg fb ... framebuffer
 * @arg name ... name
 * @arg *title ... title
 */
void choose_name(Framebuffer& fb, char name[16], const char *title)
{
    for(int i = 0; i < 15; i++) name[i] = ' ';
    name[15] = '\0';
    int position_name = 0;
    const char *rname = random_name();
    for(int i = 0; i < 16; i++) {
        if(!rname[i]) break;
        name[i] = rname[i] + ('A' - 'a');
    }

    knobs knobs;
    knobs_init(&knobs);

    for(;;) {
        knobs_update(&knobs);

        if(knobs.red_pressed) {
            while(knobs.red_pressed) { sleep_ms(10); knobs_update(&knobs); }
            break;
        }

        while(knobs.green >= 4) {
            knobs.green -= 4;
            if(name[position_name] == ' ') name[position_name] = 'A';
            else if(name[position_name] == 'Z') name[position_name] = ' ';
            else name[position_name]++;
        }

        while(knobs.green <= -4) {
            knobs.green += 4;
            if(name[position_name] == ' ') name[position_name] = 'Z';
            else if(name[position_name] == 'A') name[position_name] = ' ';
            else name[position_name]--;
        }


        while(knobs.blue >= 4) {
            knobs.blue -= 4;
            position_name++;
            if(position_name > 14) position_name = 0;
        }

        while(knobs.blue <= -4) {
            knobs.blue += 4;
            position_name--;
            if(position_name < 0) position_name = 15;
        }

        fb.fill_black();
        fb.draw_string_scaled(&font_winFreeSystem14x16, 20, 20, title, GREEN, 2);
        int posX = 80;
        int i = 0;
        for(char *s = name; *s != 0; s++) {
            if(position_name == i) {
                int width = fb.draw_character_scaled(&font_winFreeSystem14x16, posX, 80, *s, GREEN, 2);
                fb.draw_rectangle( posX, 80+16*2, posX + width, 80+16*2+4, GREEN);
                posX += width;
                i++;
            }else{
                posX += fb.draw_character_scaled(&font_winFreeSystem14x16, posX, 80, *s, RGB(0, 100, 255), 2);
                i++;
            }
        }

        fb.write_to_lcd();
        sleep_ms(10);
    }
}

struct snake_join_time {
    net_packet_join join;
    uint64_t time;
};
namespace {
    int host_end(net_packet_update update) {
        knobs knobs;
        update.data.game_over = 1;

        while(1) {
            net_broadcast(update);
            knobs_update(&knobs);
            if(knobs.red_pressed) {
                while(knobs.red_pressed) {
                    knobs_update(&knobs);
                    sleep_ms(1);
                }
                return SCREEN_MAIN_MENU;
            }
            if(knobs.green_pressed) {
                while(knobs.green_pressed) {
                    knobs_update(&knobs);
                    sleep_ms(1);
                }
                return SCREEN_MAIN_MENU;
            }
            if(knobs.blue_pressed) {
                while(knobs.blue_pressed) {
                    knobs_update(&knobs);
                    sleep_ms(1);
                }
                return SCREEN_MAIN_MENU;
            }
            sleep_ms(10);
        }
    }
    void await_press()
    {
        knobs knobs;
        while(1) {
            knobs_update(&knobs);
            if(knobs.red_pressed) {
                while(knobs.red_pressed) {
                    knobs_update(&knobs);
                    sleep_ms(1);
                }
                return;
            }
            if(knobs.green_pressed) {
                while(knobs.green_pressed) {
                    knobs_update(&knobs);
                    sleep_ms(1);
                }
                return;
            }
            if(knobs.blue_pressed) {
                while(knobs.blue_pressed) {
                    knobs_update(&knobs);
                    sleep_ms(1);
                }
                return;
            }
            sleep_ms(10);
        }
    }
}

// host game
/**
 * Does everything what is needed for hosting game 
 * @arg fb ... framebuffer
 * @return id of next screen
 */
int screen_multi_host_create(Framebuffer& fb)
{
    knobs knobs;
    knobs_init(&knobs);
    net_packet_banner banner;
    choose_name(fb, banner.data.name, "Zadejte jmeno hry:");
    std::vector<snake_join_time> joins;

    // wait for players to join
    for(;;) {
        knobs_update(&knobs);
        net_broadcast(banner);

        if(knobs.red_pressed && joins.size()) {
            while(knobs.red_pressed) { sleep_ms(10); knobs_update(&knobs); }
            break;
        }

        fb.fill_black();
        fb.draw_string_scaled(&font_winFreeSystem14x16, 20, 20, "Hosting game:", GREEN, 1);
        fb.draw_string_scaled(&font_winFreeSystem14x16, 20, 36, banner.data.name, GREEN, 2);

        // print joins
        uint16_t y = 80;
        int i = 0;
        for(snake_join_time& join : joins) {
            fb.draw_string_scaled(
                &font_winFreeSystem14x16, 20, y,
                join.join.data.player_name, 0xffff, 1
            );
            y += 20;
            i++;
        }
        fb.write_to_lcd();

        // we draw joins "ilogically" before getting them because we want to
        // render something ASAP

        // get banners
        for(;;) {
            net_packet_join join;
            ssize_t l = net_get(join);
            if(l < 0) { break; } // error getting
            if(!net_check_packet_base(&join.base, NET_TYPE_JOIN)) { continue; } // unexpected packet type
            if(join.data.player_name[15] != 0) { continue; } // malformed packet
            if(join.data.game_name[15] != 0) { continue; } // malformed packet
            std::cout << join.data.game_name << " " << join.data.player_name << std::endl;
            if(memcmp(join.data.game_name, banner.data.name, 16) != 0) { continue; } // not for us

            // do not show same banner multiple times
            bool exists = false;
            for(auto& cmp : joins) {
                if(memcmp(&join.data.player_name, &cmp.join.data.player_name, 16) == 0) {
                    cmp.join.data.knobs_value = join.data.knobs_value;
                    exists = true;
                    cmp.time = time_us();
                    break;
                }
            }
            if(exists) continue;

            snake_join_time sav;
            memcpy(&sav.join, &join, sizeof(join));
            sav.time = time_us();
            joins.push_back(std::move(sav));
        }

        // remove old joins
        uint64_t now = time_us();

        for (auto i = joins.begin(); i != joins.end();) {
            if (i->time < now - 500000)
                i = joins.erase(i);
            else
                ++i;
        }

        sleep_ms(1); // call to sleep_ms so that pc "emulation" works
    }

    fb.fill_black();
    fb.draw_centered_line(&font_winFreeSystem14x16, 100, "Starting...", 0xffff, 2);
    fb.write_to_lcd();

    // prepare game
    Game game(joins.size() + 1,48,32);
    game.start_length = 10;

    net_packet_start start;
    memcpy(start.data.game_name, banner.data.name, 16);
    start.data.player_count = game.players.size();
    struct knobs player_knobs[8];
    for(int i = 0; i < game.players.size(); i++) {
        start.data.player_colors[i] = game.players.at(i).color;
        if(i == 0) {
            knobs_init(&player_knobs[i]);
        } else {
            knobs_init_from_value(&player_knobs[i], joins[i-1].join.data.knobs_value);
        }
    }

    for(int i = 0; i < 10; i++) {
        net_broadcast(start);
        sleep_ms(100);
    }

    net_packet_update update;
    update.data.game_over = 0;
    memcpy(update.data.game_name, banner.data.name, 16);
    bool lost = false;
    for(;;) {
        knobs_update(&player_knobs[0]);
        if(!game.players.at(0).alive) {
            lost = true;
        }

        if(!lost) for(int i = 0; i < game.players.size(); i++) {
            game.players.at(i).control(&player_knobs[i]);
        }

        for(int i = 0; i < game.board.size(); i++) update.data.board[i] = game.board[i];
        net_broadcast(update);

        if(!game.players.at(0).alive) {
            lost = true;
        }

        int alivePlayerCount = 0;
        for(int i = 0; i < game.players.size(); i++) {
            if(game.players.at(i).alive) alivePlayerCount++;
        }
        if(alivePlayerCount == 1) {
            if(!lost) fb.draw_centered_line(&font_winFreeSystem14x16, 20, "You won!", RED | GREEN, 4);
            else fb.draw_centered_line(&font_winFreeSystem14x16, 20, "You died!", RED, 4);
            fb.write_to_lcd();

            return host_end(update);
        }

        fb.fill_black();
        game.update();
        game.draw(fb, 45, 30, 8);
        if(lost) fb.draw_centered_line(&font_winFreeSystem14x16, 20, "You died!", RED, 4);
        fb.write_to_lcd();

        uint64_t time_start = time_us();
        while(time_us() - time_start < 100000) {
            net_packet_knobs knobs_packet;
            if(net_get(knobs_packet) < 0) { sleep_ms(1); continue; }
            if(!net_check_packet_base(&knobs_packet.base, NET_TYPE_KNOBS)) continue;
            if(memcmp(knobs_packet.data.game_name, banner.data.name, 16) != 0) continue;
            std::cout << "recieved knobs " << knobs_packet.data.knobs << std::endl;
            for(int i = 0; i < joins.size(); i++) {
                if(memcmp(joins[i].join.data.player_name, knobs_packet.data.player_name, 16) == 0) {
                    knobs_update_from_value(&player_knobs[i + 1], knobs_packet.data.knobs);
                }
            }
            sleep_ms(1);
        }
    }
}

struct snake_banner_time {
    net_packet_banner banner;
    uint64_t time;
};
typedef struct snake_banner_time snake_banner_time;

// join game
/**
 * Does everything what is needed for join game
 * @arg fb ... framebuffer
 * @returns id of next screen 
 */
int screen_multi_host_join(Framebuffer& fb)
{
    knobs knobs;
    knobs_init(&knobs);

    std::vector<snake_banner_time> banners;

    char name[16];
    choose_name(fb, name, "Pojmenujte se:");

    uint16_t selected = 0;
    for(;;) {
        knobs_update(&knobs);

        while(knobs.red > 4) { selected++; knobs.red -= 4; }
        while(knobs.red < 4) { selected--; knobs.red += 4; }
        if(selected >= 0xff00) selected = 0;
        if(selected >= banners.size()) selected = banners.size() - 1;

        if(knobs.red_pressed && banners.size()) {
            while(knobs.red_pressed) { sleep_ms(10); knobs_update(&knobs); }
            break;
        }

        fb.fill_black();
        fb.draw_string_scaled(&font_winFreeSystem14x16, 20, 20, name, GREEN, 1);
        fb.draw_string_scaled(&font_winFreeSystem14x16, 20, 36, "Choose game:", GREEN, 2);

        // print banners
        uint16_t y = 80;
        int i = 0;
        for(auto& banner : banners) {
            if(selected == i) fb.draw_rectangle( 0, y, 10, y+10, RED);
            fb.draw_string_scaled(
                &font_winFreeSystem14x16, 20, y,
                banner.banner.data.name, 0xffff, 1
            );
            y += 20;
            i++;
        }
        fb.write_to_lcd();

        // we draw banners "ilogically" before getting them because we want to
        // render something ASAP

        // get banners
        for(;;) {
            net_packet_banner banner;
            ssize_t l = net_get(banner);
            if(l < 0) { break; } // error getting
            if(!net_check_packet_base(&banner.base, NET_TYPE_BANNER)) { continue; } // unexpected packet type
            if(banner.data.name[15] != 0) { continue; } // malformed packet

            // do not show same banner multiple times
            bool exists = false;
            for(auto& cmp : banners) {
                if(memcmp(&banner.data.name, &cmp.banner.data.name, sizeof(banner.data.name)) == 0) {
                    exists = true;
                    cmp.time = time_us();
                    break;
                }
            }
            if(exists) continue;

            snake_banner_time sav;
            memcpy(&sav.banner, &banner, sizeof(banner));
            sav.time = time_us();
            banners.push_back(std::move(sav));
        }

        // remove old banners
        uint64_t now = time_us();

        for (auto i = banners.begin(); i != banners.end();) {
            if (i->time < now - 500000)
                i = banners.erase(i);
            else
                ++i;
        }

        sleep_ms(1); // call to sleep ms so that pc "emulation" works
    }

    net_packet_join join;
    memcpy(join.data.player_name, name, 16);
    memcpy(join.data.game_name, banners.at(selected).banner.data.name, 16);

    fb.fill_black();
    fb.draw_centered_line(&font_winFreeSystem14x16, 100, "WAITING", GREEN, 2);
    fb.write_to_lcd();

    std::cout << "joining " << name << std::endl;
    net_packet_start game_init_packet;
    for(;;) { // await start packet
        join.data.knobs_value = knobs_read();
        net_broadcast(join);
        bool exit = false;
        for(;;) {
            if(net_get(game_init_packet) < 0) break; // error getting
            if(!net_check_packet_base(&game_init_packet.base, NET_TYPE_START)) continue; // bad type
            net_packet_start_data &data = game_init_packet.data;
            if(memcmp(data.game_name, join.data.game_name, 16) != 0) continue; // different game
            exit = true;
            break;
        }
        if(exit) break;

        sleep_ms(10);
    }

    Game game(game_init_packet.data.player_count, 48, 32);
    for(int i = 0; i < game.players.size(); i++) {
        game.players[i].color = game_init_packet.data.player_colors[i];
    }

    fb.fill_black();
    fb.draw_centered_line(&font_winFreeSystem14x16, 100, "Starting", GREEN, 2);
    fb.write_to_lcd();

    net_packet_knobs knobs_packet;
    memcpy(knobs_packet.data.game_name, game_init_packet.data.game_name, 16);
    memcpy(knobs_packet.data.player_name, join.data.player_name, 16);

    for(;;) {
        knobs_packet.data.knobs = knobs_read();
        net_broadcast(knobs_packet);

        net_packet_update update;
        for(;;) {
            if(net_get(update) < 0) { sleep_ms(1); continue; } // error getting

            if(!net_check_packet_base(&update.base, NET_TYPE_UPDATE)) continue; // bad type
            if(memcmp(update.data.game_name, join.data.game_name, 16) != 0) continue; // bad game.
            if(update.data.game_over) {
                fb.draw_centered_line(&font_winFreeSystem14x16, 20, "Game over", BLUE, 4);
                fb.write_to_lcd();
                await_press();
                return SCREEN_MAIN_MENU;
            }

            break;
        }
        fb.fill_black();

        for(int i = 0; i < game.board.size(); i++) {
            game.board[i] = update.data.board[i];
        }
        game.draw(fb, 45, 30, 8);

        fb.write_to_lcd();

        sleep_ms(10);
    }

    return SCREEN_MAIN_MENU;
}
