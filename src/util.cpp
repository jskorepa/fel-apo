/*
 * Copyright <2017> <Jakub Skořepa, Klára Jakešová>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "util.h"
// global variables
unsigned char *parlcd_mem_base;
unsigned char *knobs_mem_base;
const char *memdev;
#ifdef DESKTOP
uint32_t fake_rgb_knobs_value;
#endif

uint32_t knobs_read()
{
#ifndef DESKTOP
    return *(volatile uint32_t*)(knobs_mem_base + SPILED_REG_KNOBS_8BIT_o);
#else
    return fake_rgb_knobs_value;
#endif
}

void knobs_load(knobs *knobs, uint32_t rgb_knobs_value)
{
    knobs->blue_value = rgb_knobs_value & 0xff;
    knobs->green_value = rgb_knobs_value >> 8 & 0xff;
    knobs->red_value = rgb_knobs_value >> 16 & 0xff;

    knobs->blue_pressed = rgb_knobs_value >> 24 & 1;
    knobs->green_pressed = rgb_knobs_value >> 25 & 1;
    knobs->red_pressed = rgb_knobs_value >> 26 & 1;
}

void knobs_init_from_value(knobs *knobs, uint32_t value)
{
    knobs_load(knobs, knobs_read());

    knobs->blue = 0;
    knobs->green = 0;
    knobs->red = 0;
}

void knobs_init(knobs *knobs)
{
    knobs_init_from_value(knobs, knobs_read());
}

int8_t knobs_diff(uint8_t a, uint8_t b)
{
    uint8_t d = a - b;
    int8_t ret = d & 0x7f; // last 7 bits
    if(d&0x80) return -128 + ret;
    return ret;
}

void knobs_update_from_value(knobs *knobs, uint32_t kvalue)
{
    struct knobs n;
    knobs_load(&n, kvalue);

    // calculate diff
    knobs->blue += knobs_diff(n.blue_value, knobs->blue_value);
    knobs->green += knobs_diff(n.green_value, knobs->green_value);
    knobs->red += knobs_diff(n.red_value, knobs->red_value);

    // copy values
    knobs->blue_pressed = n.blue_pressed;
    knobs->green_pressed = n.green_pressed;
    knobs->red_pressed = n.red_pressed;

    knobs->blue_value = n.blue_value;
    knobs->green_value = n.green_value;
    knobs->red_value = n.red_value;
}

void knobs_update(knobs *knobs)
{
    knobs_update_from_value(knobs, knobs_read());
}

void knobs_draw(Framebuffer& fb, knobs *knobs)
{
    char s[50];
    for(int i = 0; i < sizeof(s); i++) s[i] = 0;

    sprintf(s, "diff:%d %d %d ",knobs->red, knobs->green, knobs->blue);
    fb.draw_string_scaled(&font_winFreeSystem14x16, 0, 0, s, RGB(255, 0, 0), 2);
    sprintf(s, "val:%d %d %d", knobs->red_value, knobs->green_value, knobs->blue_value);
    fb.draw_string_scaled(&font_winFreeSystem14x16, 0, 30, s, RGB(255, 0, 0), 2);
    sprintf(s, "p:%s %s %s",
      knobs->red_pressed ? "yes" : "no",
      knobs->green_pressed ? "yes" : "no",
      knobs->blue_pressed ? "yes" : "no"
    );

    fb.draw_string_scaled(&font_winFreeSystem14x16, 0, 60, s, RGB(255, 0, 0), 2);
}

#define sdl_error(p) { if(p == NULL) { printf("SDL error @ %d: %s\n", __LINE__, SDL_GetError()); exit(1);}}

void init()
{
    srand(time(NULL));
    memdev = "/dev/mem";
#ifndef DESKTOP
    printf("Running in standard mode...\n");
    parlcd_mem_base = (unsigned char *) map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
    if (parlcd_mem_base == NULL)
        exit(1);
    knobs_mem_base = (unsigned char *) map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
    if (knobs_mem_base == NULL)
        exit(1);
    parlcd_hx8357_init(parlcd_mem_base);
#else
    printf("Running in desktop mode...\n");
    parlcd_mem_base = NULL;
    knobs_mem_base = NULL;

    //The window we'll be rendering to
    SDL_Window* window = NULL;

    //The surface contained by the window
    surface = NULL;

    //Initialize SDL
    if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
    {
        printf( "SDL could not initialize! SDL_Error: %s\n", SDL_GetError() );
        exit(1);
    }

    //Create window
    window = SDL_CreateWindow("APO", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WIDTH, HEIGHT, SDL_WINDOW_SHOWN );
    sdl_error(window);

    //Get window surface
    surface = SDL_GetWindowSurface( window );
    sdl_error(surface);

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_TARGETTEXTURE);
    sdl_error(renderer);
    texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, WIDTH, HEIGHT);
    SDL_SetRenderTarget(renderer, texture);
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);
#endif
}

uint64_t time_us()
{
    struct timeval tv;
    gettimeofday(&tv,NULL);
    return 1000000 * tv.tv_sec + tv.tv_usec;
}

void sleep_ms(uint32_t ms)
{
    uint64_t t = time_us();
    #ifdef DESKTOP
    SDL_Event event;
    while (1)
    {
        while(SDL_WaitEventTimeout(&event, 1))
        {
            if (event.type == SDL_QUIT) exit(0);
            if (event.type == SDL_KEYDOWN) {
                if(event.key.repeat) continue;
                SDL_Keysym *sym = &event.key.keysym;

                if(sym->scancode == SDL_SCANCODE_D) {
                    fake_rgb_knobs_value |= 1 << 24;
                } else if(sym->scancode == SDL_SCANCODE_S) {
                    fake_rgb_knobs_value |= 1 << 25;
                } else if(sym->scancode == SDL_SCANCODE_A) {
                    fake_rgb_knobs_value |= 1 << 26;
                }
            }
            if (event.type == SDL_KEYUP) {
                SDL_Keysym *sym = &event.key.keysym;

                if(sym->scancode == SDL_SCANCODE_E) {
                    uint8_t v = (fake_rgb_knobs_value >> 0) & 0xff;
                    v -= 4;
                    fake_rgb_knobs_value &= ~(0xff << 0);
                    fake_rgb_knobs_value |= v << 0;
                } else if(sym->scancode == SDL_SCANCODE_W) {
                    uint8_t v = (fake_rgb_knobs_value >> 8) & 0xff;
                    v -= 4;
                    fake_rgb_knobs_value &= ~(0xff << 8);
                    fake_rgb_knobs_value |= v << 8;
                } else if(sym->scancode == SDL_SCANCODE_Q) {
                    uint8_t v = (fake_rgb_knobs_value >> 16) & 0xff;
                    v -= 4;
                    fake_rgb_knobs_value &= ~(0xff << 16);
                    fake_rgb_knobs_value |= v << 16;
                } else if(sym->scancode == SDL_SCANCODE_C) {
                    uint8_t v = (fake_rgb_knobs_value >> 0) & 0xff;
                    v += 4;
                    fake_rgb_knobs_value &= ~(0xff << 0);
                    fake_rgb_knobs_value |= v << 0;
                } else if(sym->scancode == SDL_SCANCODE_X) {
                    uint8_t v = (fake_rgb_knobs_value >> 8) & 0xff;
                    v += 4;
                    fake_rgb_knobs_value &= ~(0xff << 8);
                    fake_rgb_knobs_value |= v << 8;
                } else if(sym->scancode == SDL_SCANCODE_Y || sym->scancode == SDL_SCANCODE_Z) {
                    uint8_t v = (fake_rgb_knobs_value >> 16) & 0xff;
                    v += 4;
                    fake_rgb_knobs_value &= ~(0xff << 16);
                    fake_rgb_knobs_value |= v << 16;
                } else if(sym->scancode == SDL_SCANCODE_D) {
                    fake_rgb_knobs_value &= ~(1 << 24);
                } else if(sym->scancode == SDL_SCANCODE_S) {
                    fake_rgb_knobs_value &= ~(1 << 25);
                } else if(sym->scancode == SDL_SCANCODE_A) {
                    fake_rgb_knobs_value &= ~(1 << 26);
                }
            }
        }
        if((time_us() - t) > ms*1000) break;
    }
    #else
    usleep(ms*1000);
    #endif
}
